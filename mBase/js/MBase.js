/**
 * <h1>支持划屏组件的基础库</h1>
 * @module MBase
 * @author joneshe
 * @date 14-12-2 上午10:27
 */

/**
 * <h1>组件：MBase基础类</h1>
 * @class MBase
 * @example
 * <strong>实例化方法：</strong>
 * <p>var _BASE = new MBase();</p>
 * @author joneshe
 */

function MBase() {
	var that = this;

	/**
	 * 输出浏览器是否支持某些css3特性
	 * @method support
	 * @author joneshe
	 */
	that.support = {
		/**
		 * 输出浏览器是否支持transform
		 * @method support.transform
		 * @return {Boolean} 输出浏览器是否支持transform
		 * @author joneshe
		 */
		transform: (function () {
			'use strict';
			var div = document.createElement('div').style;
			return !!('transform' in div || 'webkitTransform' in div || 'msTransform' in div || 'MsTransform' in div);
		})(),

		/**
		 * 输出浏览器是否支持translate3d
		 * @method support.translate3d
		 * @return {Boolean} 输出浏览器是否支持translate3d
		 * @author joneshe
		 */
		translate3d: (function () {
			'use strict';
			var div = document.createElement('div').style;
			return !!('webkitPerspective' in div || 'MsPerspective' in div || 'perspective' in div);
		})(),

		/**
		 * 输出浏览器是否支持touch event
		 * @method support.touch
		 * @return {Boolean} 输出浏览器是否支持touch event
		 * @author joneshe
		 */
		touch: (function () {
			'use strict';
			return !!(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch);
		})(),

		/**
		 * 输出浏览器是否支持transitionEnd
		 * @method support.transitionEnd
		 * @return {Boolean} 输出浏览器是否支持transitionEnd
		 * @author joneshe
		 */
		transitionEnd: (function () {
			'use strict';
			var t,
				div = document.createElement('div'),
				which = {
					'WebkitTransition': 'webkitTransitionEnd',
					'MSTransition': 'msTransitionEnd',
					'transition': 'transitionEnd'
				};

			for (t in which) {
				if (div.style[t] !== undefined) {
					return which[t];
				}
			}
		})()
	};

	/**
	 * 检测浏览器
	 * @method browser
	 * @return {Boolean} 检测浏览器
	 * @author joneshe
	 */
	that.browser = {};

	/*
	 * touchEvent
	 * touchStart,touchMove,touchEnd
	 * 定义touch事件类型
	 * */
	var desktopEvent = [
		'mousedown',
		'mousemove',
		'mouseup'
	];

	/**
	 * 输出对应浏览器的touch事件
	 * @method touchEvent
	 * @return {String} 输出对应浏览器的touch事件
	 * @author joneshe
	 */
	that.touchEvent = {
		/**
		 * 输出对应浏览器的touchStart事件
		 * @method touchEvent.touchStart
		 * @return {String} 输出对应浏览器的touchStart事件
		 * @author joneshe
		 */
		touchStart: that.support.touch ? 'touchstart' : desktopEvent[0],

		/**
		 * 输出对应浏览器的touchMove件
		 * @method touchEvent.touchMove
		 * @return {String} 输出对应浏览器的touchMove事件
		 * @author joneshe
		 */
		touchMove: that.support.touch ? 'touchmove' : desktopEvent[1],

		/**
		 * 输出对应浏览器的touchEnd事件
		 * @method touchEvent.touchEnd
		 * @return {String} 输出对应浏览器的touchEnd事件
		 * @author joneshe
		 */
		touchEnd: that.support.touch ? 'touchend' : desktopEvent[2]
	};

	/**
	 * 输出window或者element的size大小
	 * @method help
	 * @return {Number} 输出window或者element的size大小
	 * @author joneshe
	 */
	that.help = {
		/**
		 * 输出window宽度
		 * @method help.getWinWidth
		 * @return {Number} 输出window宽度
		 * @author joneshe
		 */
		getWinWidth: function () {
			var _width;
			if (window.innerWidth) {
				_width = window.innerWidth;
			} else if (document.documentElement && document.documentElement.clientWidth) {
				_width = document.documentElement.clientWidth;
			}
			return _width;
		},

		/**
		 * 输出window高度
		 * @method help.getWinHeight
		 * @return {Number} 输出window高度
		 * @author joneshe
		 */
		getWinHeight: function () {
			var _height;
			if (window.innerHeight) {
				_height = window.innerHeight;
			} else if (document.documentElement && document.documentElement.clientHeight) {
				_height = document.documentElement.clientHeight
			}

			return _height;
		},

		/**
		 * 输出element宽度
		 * @method help.getElementWidth
		 * @return {Number} 输出element宽度
		 * @author joneshe
		 */
		getElementWidth: function (el) {
			return el.offsetWidth;
		},

		/**
		 * 输出element高度
		 * @method help.getElementHeight
		 * @return {Number} 输出element高度
		 * @author joneshe
		 */
		getElementHeight: function (el) {
			return el.offsetHeight;
		}
	};

	/**
	 * 根据传入的属性名以及值返回对应的css text
	 * @method setCssText
	 * @param {String} prop 传入的属性名
	 * @param {String} value 传入的值
	 * @return {String} 返回对应的css text
	 * @author joneshe
	 */
	that.setCssText = function (prop, value) {
		return prop + ': ' + value + '; ';
	};

	/**
	 * 根据传入的属性名以及值输出对应的css text
	 * @method insertCss
	 * @param {String} rule 传入的css text
	 * @return {Boolean} 传入不为空则返回true，反则返回false
	 * @author joneshe
	 */
	that.insertCss = function (rule) {
		if (rule === '') {
			return false;
		}

		var head = document.head || document.getElementsByTagName('head')[0],
			style;

		if (!!head.getElementsByTagName('style').length) {
			style = head.getElementsByTagName('style')[0];
			if (style.styleSheet) {
				style.styleSheet.cssText = rule;
			} else {
				style.appendChild(document.createTextNode(rule));
			}
		} else {
			style = document.createElement('style');

			style.type = 'text/css';
			if (style.styleSheet) {
				style.styleSheet.cssText = rule;
			} else {
				style.appendChild(document.createTextNode(rule));
			}

			head.appendChild(style);
		}

		return true;
	};

	/**
	 * 事件防抖（函数节流）
	 * @method debounce
	 * @param {Function} func 传入function
	 * @param {Number} wait 间隔时间
	 * @param {Boolean} [immediate] 传参immediate为true，debounce会在wait时间间隔的开始调用这个函数。并且在wait的时间之内，不会再次调用。
	 * @return {Function} 返回传入的function
	 * @author joneshe
	 */
	that.debounce = function (func, wait, immediate) {
		var timeout, result;
		return function () {
			var context = this, args = arguments;
			var later = function () {
				timeout = null;
				if (!immediate) result = func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) result = func.apply(context, args);
			return result;
		};
	};

	that.hasClass = function (el, className) {
		if (el.classList)
			return el.classList.contains(className);
		else
			return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
	};

	that.addClass = function (el, className) {
		if (el.classList)
			el.classList.add(className);
		else if (!that.hasClass(el, className)) el.className += " " + className
	};

	that.removeClass = function (el, className) {
		if (el.classList)
			el.classList.remove(className);
		else if (that.hasClass(el, className)) {
			var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
			el.className = el.className.replace(reg, ' ');
		}
	};

	/**
	 * 设置动画时长
	 * @method setTransitionDuration
	 * @param {Object} el 需要设置的element
	 * @param {Number} duration 动画持续时间
	 * @author joneshe
	 */
	that.setTransitionDuration = function (el, duration) {
		var es = el.style;
		es.webkitTransitionDuration = es.MsTransitionDuration = es.msTransitionDuration = es.transitionDuration = duration / 1000 + 's';
	};

	var navigationOptions = {};

	that.initNavigation = function (opt) {
		var navHtml = '';

		navigationOptions = opt;

		for (var i = 0; i < opt.total; i++) {

			navHtml += (i === 0) ? '<a class="' + opt.navChildClassName + ' ' + opt.navChileCurrentClassName + '" href="javascript:;">' + (i + 1) + '</a>' : '<a class="' + opt.navChildClassName + '" href="javascript:;">' + (i + 1) + '</a>'
		}

		var _nav = document.createElement('div');
		_nav.className = opt.navClassName;
		_nav.innerHTML = navHtml;

		var _parent = opt.selector.parentNode;
		_parent.appendChild(_nav);
		
		that.navItem = _nav.childNodes;
		that.currentNavItem = that.navItem[0];

		if (!!opt.navigationAllowClick) {
			var _opt = opt;

			_opt.selector = _nav;
			_eventNavigation(_opt);
		}

		function _eventNavigation() {
			opt.selector.addEventListener(that.touchEvent.touchEnd, function (e) {
				if (e.target.className.split(opt.navChildClassName).length >= 2) {

					if (e.target !== that.currentNavItem) {
						that.toggleNavigation(e.target);

						opt.onChangeEnd(that.index(e.target));
					}
				}
			}, false);
		}

	};

	that.toggleNavigation = function (el, opt) {
		opt = opt || navigationOptions;

		if (el !== that.currentNavItem) {
			that.removeClass(that.currentNavItem, opt.navChileCurrentClassName);
			that.addClass(el, opt.navChileCurrentClassName);
			that.currentNavItem = el;

		}
	};

	that.index = function (el) {
		var _all = el.parentNode.childNodes;

		var _len = _all.length;
		for (var i = 0; i < _len; i++) {
			if (el === _all[i]) {
				return i
			}
		}
	};

}

MBase.prototype = {
	constructor: MBase
};