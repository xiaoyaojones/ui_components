function Slide(selector, params) {

	if (typeof selector === 'undefined') return;

	var that = this;

	var defaults = {
		className: 'layout',
		childClassName: 'layout-page',
		containerClassName: 'container',
		activePage: 0,

		direction: 'y',
		/**
		 * 是否全屏slide
		 * @attribute fullScreen
		 * @type Boolean
		 * @default true
		 * @author joneshe
		 */
		fullScreen: true,
		/**
		 * 是否允许到头、尾时还可被拖动
		 * @attribute allowEndBounce
		 * @type Boolean
		 * @default true
		 * @author joneshe
		 */
		allowEndBounce: false,
		/**
		 * touchmove时，版面是否跟随手指移动
		 * @attribute followFinger
		 * @type Boolean
		 * @default false
		 * @author joneshe
		 */
		followFinger: true,
		/**
		 * touchmove时，版面跟随手指的移动幅度
		 * @attribute touchRatio
		 * @type Number
		 * @default 1.2
		 * @author joneshe
		 */
		touchRatio: 1.2,
		/**
		 * 移动幅度达到页面宽度的多少比例即可切换
		 * @attribute swipeRatio
		 * @type Number
		 * @default 0.3
		 * @author joneshe
		 */
		swipeRatio: 0.3,
		/**
		 * 移动阀值，达到该数值以上即可切换页面
		 * @attribute swipeRatio
		 * @type Number
		 * @default 80
		 * @author joneshe
		 */
		moveStartThreshold: 80,
		/**
		 * 切换页面的动画间隔
		 * @attribute speed
		 * @type Number
		 * @default 300
		 * @author joneshe
		 */
		speed: 300,

		navigation: false,

		/**
		 * 页码父元素className
		 * @attribute navClassName
		 * @type String
		 * @default 'layout-nav'
		 * @author joneshe
		 */
		navClassName: 'layout-nav',
		/**
		 * 页码子元素className
		 * @attribute navChildClassName
		 * @type String
		 * @default 'layout-nav_item'
		 * @author joneshe
		 */
		navChildClassName: 'layout-nav_item',
		/**
		 * 当前页码高亮className
		 * @attribute navChileCurrentClassName
		 * @type String
		 * @default 'layout-nav_item__current'
		 * @author joneshe
		 */
		navChileCurrentClassName: 'layout-nav_item__current',

		/**
		 * 页码是否能被点击
		 * @attribute navigationAllowClick
		 * @type Boolean
		 * @default false
		 * @author joneshe
		 */
		navigationAllowClick: false,

		buildAnimate: true,

		buildAnimateJs: '../mBase/js/MFx.js',

		/**
		 * onTouchStart触发的callback
		 * @attribute onTouchStartCB
		 * @type Function
		 * @default null
		 * @author joneshe
		 */
		onTouchStartCB: null,
		/**
		 * onTouchEnd触发的callback
		 * @attribute onTouchEndCB
		 * @type Function
		 * @default null
		 * @author joneshe
		 */
		onTouchEndCB: null,
		/**
		 * 切换页面前触发的callback
		 * @attribute onChangeStartCB
		 * @type Function
		 * @default null
		 * @author joneshe
		 */
		onChangeStartCB: null,
		/**
		 * 切换页面后触发的callback
		 * @attribute onChangeEndCB
		 * @type Function
		 * @default null
		 * @author joneshe
		 */
		onChangeEndCB: null,
		/**
		 * touching组件时触发的callback
		 * @attribute onTouchMove
		 * @type Function
		 * @default null
		 * @author joneshe
		 */
		onTouchMove: null,

		stopMove: null
	};

	params = params || {};
	for (prop in defaults) {
		if (prop in params && typeof params[prop] === 'object') {
			for (subProp in defaults[prop]) {
				if (!(subProp in params[prop])) {
					params[prop][subProp] = defaults[prop][subProp];
				}
			}
		}
		else if (!(prop in params)) {
			params[prop] = defaults[prop];
		}
	}

	that.selector = document.getElementById(selector);

	that.activePage = params.activePage;

	var posGrid = [];

	var containerSize,          //container 宽/高
		layoutSize;              //slide 总宽/高

	var insMBase = new MBase();

	var _MNS = {};
	_MNS.id = selector;
	_MNS.childList = that.selector.querySelectorAll('.' + params.childClassName);
	_MNS.childListLength = _MNS.childList.length;

	/*设置调用that.setTranslate的相关参数*/
	var setTransOption = {
		'direction': params.direction,
		'allowEndBounce': params.allowEndBounce
	};

	that.removeDefaultMove = function (e){
    e.preventDefault();
	};

	that.init = function () {
		var _container = document.getElementById(params.containerClassName),
			_html = document.getElementsByTagName('html')[0],
			_event = insMBase.touchEvent,
			_cssText = '';

		window.scrollTo(0, 1);

		_html.addEventListener(_event.touchMove, that.removeDefaultMove, false);

		if (params.fullScreen) {
			_container.style.height = insMBase.help.getWinHeight() + 'px';
		}

		_cssText += _setFWSize();

		insMBase.insertCss(_cssText);

		_initEvents();

		setTransOption.maxPosition = that.getMaxPosition();

		if (params.navigation) {
			insMBase.initNavigation({
				selector: that.selector,

				active: params.activePage,

				total: _MNS.childListLength,

				navClassName: params.navClassName,

				navChildClassName: params.navChildClassName,

				navChileCurrentClassName: params.navChileCurrentClassName,

				navigationAllowClick: params.navigationAllowClick,

				onChangeEnd: that.swipeTo
			});
		}


		if (params.activePage !== 0) {
			that.swipeTo(params.activePage);
		}

		if (params.buildAnimate) {
			// _asyncInitAnimate();

			insMBase.addClass(that.outputParams().activePageDom, 'active');

		}

	};

	/**
	 * 初始化时设置slide框架size
	 * @method _setFWSize
	 * @static
	 * @return {String} 返回一串css字符
	 * @author joneshe
	 */

	function _setFWSize() {
		var _width,
			_height,
			cssText = '',
			sTranslate = insMBase.support.translate3d ? 'translate3d(0px,0px,0px)' : 'translate(0px,0px)';

		cssText += '.' + params.className + '{ ' +
			//insMBase.setCssText('-webkit-transition-attribute', 'transform, left, top') +
			insMBase.setCssText('-webkit-transition-duration', '0s') +
			insMBase.setCssText('transition-duration', '0s') +
			insMBase.setCssText('-webkit-transition-timing-function', 'ease') +
			insMBase.setCssText('transition-timing-function', 'ease') +
			insMBase.setCssText('-webkit-transform', sTranslate) +
			insMBase.setCssText('transform', sTranslate) +
			'}';

		if (params.direction === 'y') {
			//竖向滚动
			cssText += '#' + _MNS.id + '{ ' +
				insMBase.setCssText('width', '100%') +
				insMBase.setCssText('height', (_MNS.childListLength * 100) + '%') +
				'}' +
				'#' + _MNS.id + ' > .' + params.childClassName + '{' +
				insMBase.setCssText('width', '100%') +
				insMBase.setCssText('height', (100 / _MNS.childListLength) + '%') +
				'}';

			_height = (params.fullScreen ? insMBase.help.getWinHeight() : insMBase.help.getElementHeight(that.selector.parent()));
			layoutSize = _height * _MNS.childListLength;
			containerSize = _height;
		} else {
			//横向滚动
			cssText += '#' + _MNS.id + '{ ' +
				insMBase.setCssText('width', (_MNS.childListLength * 100) + '%') +
				insMBase.setCssText('height', '100%') +
				insMBase.setCssText('display', '-webkit-box') +
				insMBase.setCssText('display', '-webkit-flex') +
				insMBase.setCssText('display', 'flex') +
				'}' +
				'#' + _MNS.id + ' > .' + params.childClassName + '{' +
				insMBase.setCssText('width', '100%') +
				insMBase.setCssText('height', '100%') +
				insMBase.setCssText('-webkit-box-flex', 1) +
				insMBase.setCssText('-webkit-flex', 1) +
				insMBase.setCssText('flex', 1) +
				'}';

			_width = (params.fullScreen ? insMBase.help.getWinWidth() : insMBase.help.getElementWidth(that.selector.parent()));
			layoutSize = _width * _MNS.childListLength;
			containerSize = _width;
		}

		var _position = 0;
		for (var i = 0; i < _MNS.childListLength; i++) {
			_position = i * containerSize;
			posGrid.push(_position);
		}

		return cssText;

	}

  var _fnOnChangeEndCB;

  that.onChangeEndCB = function (fn) {

    if (typeof fn === 'function') {
      fn.call(this, that.outputParams());

      _fnOnChangeEndCB = fn;
    }

  };

	/**
	 * touch事件初始化
	 * @event _initEvents
	 * @type Function
	 * @static
	 * @author joneshe
	 */
	function _initEvents() {
    that.resetAddEvents();

		that.selector.addEventListener(insMBase.support.transitionEnd, function (e) {
			if (typeof params.onChangeEndCB === 'function') {

				var _outputParams = that.outputParams();

				if (!!params.buildAnimate) {
					insMBase.removeClass(that.oldPageDom, 'active');
					insMBase.addClass(_outputParams.activePageDom, 'active');
				}


        if (typeof _fnOnChangeEndCB === 'function') {
          _fnOnChangeEndCB.call(this, that.outputParams());
        }else{
          params.onChangeEndCB(_outputParams);
        }

        // console.log(that.onChangeEndCB);

			}
		}, false);
	}

  /**
   * 移除所有touch事件
   */
	that.removeEvents = function (){
    that.selector.removeEventListener(insMBase.touchEvent.touchStart, onTouchStart, false);

    that.selector.removeEventListener(insMBase.touchEvent.touchMove, onTouchMove, false);

    that.selector.removeEventListener(insMBase.touchEvent.touchEnd, onTouchEnd, false);
	};

  /**
   * 重新绑定touch事件
   */
	that.resetAddEvents = function (){
    that.selector.addEventListener(insMBase.touchEvent.touchStart, onTouchStart, false);

    that.selector.addEventListener(insMBase.touchEvent.touchMove, onTouchMove, false);

    that.selector.addEventListener(insMBase.touchEvent.touchEnd, onTouchEnd, false);
	};

	function _asyncInitAnimate() {
		var s = document.createElement('script');
		//s.async = true;
		s.src = params.buildAnimateJs;
		document.getElementsByTagName('body')[0].appendChild(s);

		s.onload = function () {
			var insMFx = new MFx();
		};
	}

	var isTouched = false,
		isMoving = false;

	var isTouchEvent = false,
		allowThresholdMove,
		isHorizontal = params.direction === 'x',
		isScrolling;

	/*
	 * 定义event中所需变量
	 * */
	var touchValue = {
			start: 0,
			startX: 0,
			startY: 0,
			current: 0,
			currentX: 0,
			currentY: 0,
			diff: 0,
			abs: 0
		},
		positionValue = {
			start: 0,
			abs: 0,
			diff: 0,
			current: 0
		},
		timeValue = {
			start: 0,
			end: 0
		};

	that.outputParams = function () {
		return {
			selector: that.selector,
			activePage: that.activePage,
			activePageDom: that.selector.children[that.activePage],
			oldPageDom: that.oldPageDom,
			touchValue: touchValue,
			positionValue: positionValue,
			timeValue: timeValue
		}
	};

	//切换前的page
	that.oldPageDom = that.selector.children[that.activePage];

	/**
	 * touchstart事件定义
	 * @event onTouchStart
	 * @param {Event} e event对象
	 * @type Function
	 * @static
	 * @author joneshe
	 */
	function onTouchStart(e) {
		if (isTouched) {
			return false;
		}

		isTouched = true;

		if (typeof params.onTouchStartCB === 'function') {
			params.onTouchStartCB(that.outputParams());
		}

		isTouchEvent = e.type === 'touchstart';

		var touches = e.targetTouches;

		var pageX = isTouchEvent ? touches[0].pageX : (e.pageX || e.clientX),
			pageY = isTouchEvent ? touches[0].pageY : (e.pageY || e.clientY);

		touchValue.startX = touchValue.currentX = pageX;
		touchValue.startY = touchValue.currentY = pageY;

		touchValue.start = touchValue.current = isHorizontal ? pageX : pageY;

		positionValue.start = positionValue.current = that.getTranslate(that.selector, params.direction);

		insMBase.setTransitionDuration(that.selector, 0);

		//设置动画
		that.setTranslate(that.selector, setTransOption, positionValue.start);

		//记录touchstart开始时间
		timeValue.start = (new Date()).getTime();

		//重设isScrolling状态
		isScrolling = undefined;

		if (params.moveStartThreshold > 0) {
			allowThresholdMove = false;
		}

	}

	/**
	 * onTouchMove
	 * @event onTouchStart
	 * @param {Event} e event对象
	 * @author joneshe
	 * @type Function
	 * @static
	 */
	function onTouchMove(e) {
		if (!isTouched) {
			return;
		}

		console.log(e);

		if (!!params.stopMove) {
			var _len = params.stopMove.length;
			for (var i = 0; i < _len; i++) {
				if (params.stopMove[i] === that.activePage) {
					return;
				}
			}
		}


		if (isTouchEvent && e.type === 'mousemove') {
			return;
		}

		var touches = e.targetTouches;

		var pageX = isTouchEvent ? touches[0].pageX : (e.pageX || e.clientY),
			pageY = isTouchEvent ? touches[0].pageY : (e.pageY || e.clientY);

		if (typeof isScrolling === 'undefined' && isHorizontal) {
			isScrolling = !!(isScrolling || Math.abs(pageY - touchValue.startY) > Math.abs(pageX - touchValue.startX));
		}

		if (typeof isScrolling === 'undefined' && !isHorizontal) {
			isScrolling = !!(isScrolling || Math.abs((pageY - touchValue.startX)) < Math.abs(pageX - touchValue.startX));
		}

		if (!!isScrolling) {
			isTouched = false;
			return;
		}

		//fixed：嵌套模式下可以随意拖动
		//if (!!e.nested) {
		//	isTouched = false;
		//	return;
		//}
		//
		//e.nested = true;

		isMoving = true;

		e.preventDefault();

		touchValue.current = isHorizontal ? pageX : pageY;

		positionValue.current = (touchValue.current - touchValue.start) * params.touchRatio + positionValue.start;

		if (Math.abs(touchValue.current - touchValue.start) > params.moveStartThreshold || allowThresholdMove) {

			if (!allowThresholdMove) {
				allowThresholdMove = true;
				touchValue.start = touchValue.current;
				return;
			}

			if (!!params.followFinger) {
				insMBase.debounce(that.setTranslate(that.selector, setTransOption, positionValue.current), 16);
			}
		} else {
			positionValue.current = positionValue.start;
		}

		if (typeof params.onTouchMove === 'function') {
			params.onTouchMove(that.outputParams());
		}

	}

	/**
	 * touchend事件定义
	 * @event onTouchEnd
	 * @param {Event} e event对象
	 * @type Function
	 * @static
	 * @author joneshe
	 */
	function onTouchEnd(e) {
		if (isScrolling) {
			that.swipeReset();
		}

		isTouched = false;

		if (!positionValue.current && positionValue.current !== 0) {
			positionValue.current = positionValue.start;
		}

		timeValue.end = (new Date()).getTime();

		touchValue.diff = touchValue.current - touchValue.start;
		touchValue.abs = Math.abs(touchValue.diff);

		positionValue.diff = positionValue.current - positionValue.start;
		positionValue.abs = Math.abs(positionValue.diff);

		var diff = positionValue.diff,
			diffAbs = positionValue.abs,
			timeDiff = timeValue.end - timeValue.start;

		var direction = diff < 0 ? 'next' : 'prev';

		//short touch
		if (timeDiff <= 300) {
			if (diffAbs < 5) {
				if (diffAbs === 0) {
					that.swipeReset();
				}
			} else {
				if (direction === 'next') {
					if (diffAbs < 30) {
						that.swipeReset();
					} else {
						that.swipeNext();
					}
				}
				if (direction === 'prev') {
					if (diffAbs < 30) {
						that.swipeReset();
					} else {
						that.swipePrev();
					}
				}
			}

		}
		//long touch
		else {
			if (direction === 'next') {
				if (diffAbs >= containerSize * params.swipeRatio) {
					that.swipeNext();
				} else {
					that.swipeReset();
				}
			}
			if (direction === 'prev') {
				if (diffAbs >= containerSize * params.swipeRatio) {
					that.swipePrev();
				} else {
					that.swipeReset();
				}
			}
		}

		if (typeof params.onTouchEndCB === 'function') {
			params.onTouchEndCB(that.outputParams());
		}

	}

	/**
	 * 获取组件最大偏移量
	 * @method getMaxPosition
	 * @return {Number} 返回最大偏移量
	 * @author joneshe
	 */
	that.getMaxPosition = function () {
		var a = layoutSize - containerSize;
		if (a < 0) a = 0;
		return a;
	};

	/**
	 * 切换到下一屏
	 * @method swipeNext
	 * @return {Boolean} 返回是否转到下一屏结果
	 * @author joneshe
	 */
	that.swipeNext = function () {
		var currentPosition = that.getTranslate(that.selector, params.direction),
			newPosition;

		newPosition = -(Math.floor(Math.abs(currentPosition) / Math.floor(containerSize)) * containerSize + containerSize);

		newPosition = newPosition < -that.getMaxPosition() ? -that.getMaxPosition() : newPosition;

		if (newPosition === currentPosition) {
			return false;
		}

		//if (typeof params.onChangeStartCB === 'function') {
		//	params.onChangeStartCB(that.activePage);
		//}

		that.oldPageDom = that.selector.children[that.activePage];

		that.activePage = that.activePage >= _MNS.slideLength - 1 ? _MNS.slideLength - 1 : -newPosition / containerSize;

		swipeToPosition(newPosition, 'next');

		return true;
	};

	/**
	 * 切换到上一屏
	 * @method swipePrev
	 * @return {Boolean} 返回是否转到上一屏结果
	 * @author joneshe
	 */
	that.swipePrev = function () {
		var currentPosition = that.getTranslate(that.selector, params.direction),
			newPosition;

		newPosition = -(Math.ceil(-currentPosition / containerSize) - 1) * containerSize;

		newPosition = newPosition > 0 ? 0 : newPosition;

		if (newPosition === currentPosition) {
			return false;
		}

		//if (typeof params.onChangeStartCB === 'function') {
		//	params.onChangeStartCB(that.activePage);
		//}

		that.oldPageDom = that.selector.children[that.activePage];

		that.activePage = that.activePage <= 0 ? 0 : -newPosition / containerSize;

		swipeToPosition(newPosition, 'prev');

		return true;
	};

	/**
	 * 重置当前屏
	 * @method swipePrev
	 * @return {Boolean} 是否重置当前屏
	 * @author joneshe
	 */
	that.swipeReset = function () {

		var currentPosition = that.getTranslate(that.selector, params.direction),
			newPosition,
			maxPosition = -that.getMaxPosition();

		newPosition = 0;

		for (var i = 0; i < posGrid.length; i++) {
			if (-currentPosition === posGrid[i]) {
				return
			}

			if (-currentPosition >= posGrid[i] && -currentPosition < posGrid[i + 1]) {
				newPosition = positionValue.diff > 0 ? -posGrid[i + 1] : -posGrid[i];
				break;
			}
		}

		newPosition = -currentPosition >= posGrid[posGrid.length - 1] ? -posGrid[posGrid.length - 1] : newPosition;

		newPosition = currentPosition <= maxPosition ? maxPosition : newPosition;

		if (newPosition === currentPosition) {
			return false;
		}

		swipeToPosition(newPosition, 'reset');
		return true;
	};

	/**
	 * 设置偏移量
	 * @event swipeToPosition
	 * @param {Number} newPosition 新偏移量
	 * @param {String} action 当前操作属于哪个动作（待增加行为处理）
	 * @type Function
	 * @static
	 * @author joneshe
	 */
	function swipeToPosition(newPosition, action) {
		var speed = params.speed;

		if (typeof params.onChangeStartCB === 'function') {
			params.onChangeStartCB(that.outputParams());
		}

		that.setTranslate(that.selector, setTransOption, newPosition);
		insMBase.setTransitionDuration(that.selector, speed);

		if (!!params.navigation) {
			insMBase.toggleNavigation(insMBase.navItem[that.activePage]);
		}

		//if (typeof params.onChangeEndCB === 'function') {
		//	params.onChangeEndCB(that.outputParams());
		//}

	}

	/**
	 * 定位到某一页（会出现滚动效果），index从0开始
	 * @method swipeTo
	 * @param {Number} index index从0开始
	 * @return {Boolean} 是否完成定位
	 * @author joneshe
	 */
	that.swipeTo = function (index) {
		index = parseInt(index, 10);

		if (index > _MNS.slideLength - 1 || index < 0) {
			return;
		}

		var currentPosition = that.getTranslate(that.selector, params.direction),
			newPosition;

		newPosition = -index * containerSize;

		newPosition = (newPosition < -that.getMaxPosition()) ? -that.getMaxPosition() : newPosition;

		if (newPosition === currentPosition) {
			return false;
		}

		that.oldPageDom = that.selector.children[that.activePage];

		that.activePage = index;

		swipeToPosition(newPosition, 'swipeTo');

		return true;
	};

	/**
	 * 获取element偏移量
	 * @method getTranslate
	 * @param {Object} el DOM对象，非$对象
	 * @param {String} axis duration x/y 方向
	 * @return {Number} 返回对象的translate值
	 * @author joneshe
	 */
	that.getTranslate = function (el, axis) {
		var bHasCssMatrix = window.WebKitCSSMatrix,
			matrix, currTransform, currStyle, transformMatrix;

		if (!!insMBase.support.transform) {
			currStyle = window.getComputedStyle(el);

			if (bHasCssMatrix) {
				transformMatrix = new WebKitCSSMatrix(currStyle.webkitTransform === 'none' ? '' : currStyle.webkitTransform);
			} else {
				transformMatrix = currStyle.MsTransform || currStyle.msTransform || currStyle.transform || currStyle.getPropertyValue('transform').replace('translate(', 'matrix(1, 0, 0, 1');
				matrix = transformMatrix.toString().split(',');
			}

			if (axis === 'x') {
				if (bHasCssMatrix) {
					currTransform = transformMatrix.m41;
				} else if (matrix.length === 16) {
					currTransform = parseFloat(matrix[12]);
				} else {
					currTransform = parseFloat(matrix[4]);
				}
			}

			if (axis === 'y') {
				if (bHasCssMatrix) {
					currTransform = transformMatrix.m42;
				} else if (matrix.length === 16) {
					currTransform = parseFloat(matrix[13]);
				} else {
					currTransform = parseFloat(matrix[5]);
				}
			}

		} else {
			if (axis === 'x') {
				currTransform = parseFloat(el.style.left, 10) || 0;
			}
			if (axis === 'y') {
				currTransform = parseFloat(el.style.top, 10) || 0;
			}
		}
		return currTransform || 0;

	};

	/**
	 * 设置element偏移量
	 * @method setTranslate
	 * @param {Object} el DOM对象，非$对象
	 * @param {Object} opt 设置项（direction、allowEndBounce、maxPosition）
	 * @param {String} x x轴偏移量(px)，若只传入3个参数，并传入的opt.direction值为y，该参数将为y轴偏移量(px)
	 * @param {String} [y] y轴偏移量(px)
	 * @author joneshe
	 */
	that.setTranslate = function (el, opt, x, y) {
		var es = el.style,
			xyz = {
				x: 0,
				y: 0,
				z: 0
			},
			translate;

		if (arguments.length === 4) {
			xyz.x = x;
			xyz.y = y;
		} else {
			if (typeof y === 'undefined') {
				y = opt.direction == 'x' ? 'x' : 'y'
			}
			if (!opt.allowEndBounce) {
				x = (x >= 0) ? 0 : x;
				x = (x < -opt.maxPosition) ? -opt.maxPosition : x;
			}
			xyz[y] = x;
		}

		if (!!insMBase.support.transform) {
			translate = insMBase.support.translate3d ? 'translate3d(' + xyz.x + 'px, ' + xyz.y + 'px, ' + xyz.z + 'px)' : 'translate(' + xyz.x + 'px, ' + xyz.y + 'px)';
			es.webkitTransform = es.MsTransform = es.msTransform = es.transform = translate;
		} else {
			es.left = xyz.x + 'px';
			es.top = xyz.y + 'px';
		}

	};

  /**
   * 销毁禁止滚动方法
   */
	that.destroy = function (){
    document.getElementsByTagName('html')[0].removeEventListener(insMBase.touchEvent.touchMove, that.removeDefaultMove, false);
	}

}

Slide.prototype = {
	constructor: Slide
};